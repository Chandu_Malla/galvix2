import React from 'react'
import Header from './Components/Header'
import SalesTaxFilingSection from './Components/SalesTaxFilingSection'
import NewsLetterSection from './Components/NewsLetterSection'
import FeatureSection from './Components/FeatureSection'

function App() {
  return (
    <div>
      <Header />
      <SalesTaxFilingSection />
      <FeatureSection />
      <NewsLetterSection />
    </div>
  )
}

export default App
